var app = {  
  
    initialize: function() {  
        this.bindEvents();  
    },  
     
    bindEvents: function() {  
        var takePhoto = document.getElementById('takePhoto');  
        takePhoto.addEventListener('click', app.takePhoto, false);  
        var sendPhoto = document.getElementById('sendPhoto');  
        sendPhoto.addEventListener('click', app.sendPhoto, false);  
    },  
    
     selectPicture: function () {
        navigator.camera.getPicture(
            function(uri) {
                var img = document.getElementById('uploadedFile');
                img.style.visibility = "visible";
                img.style.display = "block";
                img.src = uri;
                document.getElementById('camera_status').innerHTML = "Success";
            },
            function(e) {
                console.log("Error getting picture: " + e);
                document.getElementById('camera_status').innerHTML = "Error getting picture.";
            },
            { quality: 50, destinationType: navigator.camera.DestinationType.FILE_URI, sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY});
    },
  
    sendPhoto: function() { 
    	$.mobile.loading("show", {
                    text : 'Enviando correo',
                    textVisible : true,
                    theme : 'z',
                    textonly : false,
                    html : ""
                });         
    	//alert("1");
        var img = document.getElementById('uploadedFile');
        var fileURL = img.src;
      //  alert("2");
      function win(r) {
      		$.mobile.loading('hide');
      		cambioPagina('contacto3');
      		$("#avisoResultadosContacto2").html("");
 			$("#avisoResultadosContacto2").removeClass("alert alert-danger");
 			$("#avisoResultadosContacto2").removeClass("alert alert-success");
      	 
          	sAviso = "<p class='denuncia'><b>Se ha enviado un mail con exito a nuestra oficina. En caso de ser un caso urgente, es mejor llamar por teléfono</b></p>";
          	sAviso1 = "En unos instantes recibir&aacute;s un correo confirm&aacute;ndote la informaci&oacute;n que acabas de enviar .A partir de ahora nuestro equipo de profesionales, trabajaran para confeccionar un presupuesto sobre lo que te costara libarte de tu multa.<br>";
          	sAviso1 = sAviso1+"Este presupuesto es gratuito y si compromiso, en hemos llegado a hacer ahorrar hasta un 85% del importe de la multa.<br>";
          	sAviso1 = sAviso1+"Gracias por tu confianza";
          	$("#avisoResultadosContacto2").html(sAviso);
          	$("#avisoResultadosContacto3").html(sAviso1);
          	$("#avisoResultadosContacto2").addClass("alert alert-success");
        	$.mobile.loading('hide');
		}
		//alert("3");
		function fail(error) {
		    $.mobile.loading('hide');
		    cambioPagina('contacto3');
		   	$("#avisoResultadosContacto2").html("");
			$("#avisoResultadosContacto2").removeClass("alert alert-danger");
                      	 
          sAviso = "<p class='denuncia'>Por razones tecnicas transitorias, no nos es posible ofercer la informaci&oacute;n solicitada. Intentalo de nuevo m&aacute;s tarde</p>";
          $("#avisoResultadosContacto2").html(sAviso);
          $("#avisoResultadosContacto2").addClass("alert alert-danger");
          $.mobile.loading('hide');
		}
		
		//var uri = encodeURI("http://10.0.2.2:8889/rest/mail");
		var uri = encodeURI("http://multasradar4.appspot.com/rest/mail");
		//alert("4");
		var options = new FileUploadOptions();

        options.fileKey="file";
        options.fileName=fileURL.substr(fileURL.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";
        //alert("4.0");
        options.chunkedMode = false;
        options.headers = {
           Connection: "close"
        };

		var email = $("#email").val()
		var telf = $("#telf").val();
		var calendario = $("#calendario").val();

        var params = {};
        params.calendario = calendario;
        params.telf =telf;
        params.email = email;
        params.textarea = $("#textarea-1").val();
		options.params= params;
		
		if(email=="" || !validarEmail( email )){
					 	$("#avisoResultadosContacto").html("<p>Es obligatorio un correo electronico valido para poder avisarte</p>");
					 	
					 	$("#avisoResultadosContacto").addClass("alert alert-danger");
					 	
					 	 $.mobile.loading('hide');
					 	return null;
					}	
		
		
		var headers={'headerParam':'headerValue'};
		
		options.headers = headers;
		//	alert("6");
		var ft = new FileTransfer();
		//alert("7");
		ft.onprogress = function(progressEvent) {
		    if (progressEvent.lengthComputable) {
		      loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
		    } else {
		      loadingStatus.increment();
		    }
		};
		//alert("Enviando " );
		ft.upload(fileURL, uri, win, fail, options);
		//alert("Enviando 1" );
    },  
    
     /* win: function (r) {
      		$.mobile.loading('hide');
      		alert("Enviado esta ");
            console.log("Code = " + r.responseCode);
            console.log("Response = " + r.response);
            console.log("Sent = " + r.bytesSent);
       },

      fail:  function (error) {
      		$.mobile.loading('hide');
            alert("An error has occurred: Code = " + error.code);
            alert("r.response = " + r.response);
            console.log("upload error source " + error.source);
            console.log("upload error target " + error.target);
       },*/
  
    takePhoto: function(){  
    	//alert("take photo");
        navigator.camera.getPicture(app.onPhotoDataSuccess, app.onFail, { quality: 20,   
            allowEdit: true, destinationType: navigator.camera.DestinationType.DATA_URL });  
    },  
  
    onPhotoDataSuccess: function(imageData) {  
    		$.mobile.loading("show", {
                    text : 'Adjuntando foto',
                    textVisible : true,
                    theme : 'z',
                    textonly : false,
                    html : ""
                }); 
     
      var photo = document.getElementById('uploadedFile');  
  
      photo.style.display = 'block';  
  
      photo.src = "data:image/jpeg;base64," + imageData;  
  
      var sendPhoto = document.getElementById('sendPhoto');  
      sendPhoto.style.display = 'block';  
      var siguiente = document.getElementById('siguiente');  
      siguiente.style.display = 'block';  
       $.mobile.loading('hide');
        cambioPagina('contacto');
    },
  
    onFail: function(message) {  
      alert('Fallo al poner en funcionamiento la camara: ' + message);  
    }  
  
};